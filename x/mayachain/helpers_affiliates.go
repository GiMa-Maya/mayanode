package mayachain

import (
	"fmt"

	"github.com/blang/semver"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/constants"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

// addSwapDirect adds the swap directly to the swap queue (no order book) - segmented
// out into its own function to allow easier maintenance of original behavior vs order
// book behavior.
func addSwapDirect(ctx cosmos.Context, mgr Manager, msg MsgSwap) {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.112.0")):
		addSwapDirectV112(ctx, mgr, msg)
	default:
		ctx.Logger().Error("addSwapDirect invalid version %s", version.String())
	}
}

func addSwapDirectV112(ctx cosmos.Context, mgr Manager, msg MsgSwap) {
	if msg.Tx.Coins.IsEmpty() {
		return
	}

	totalAffFee := msg.GetTotalAffiliateFee()
	if !totalAffFee.IsZero() {
		// Distribute affiliate fees
		totalAffiliateFee, err := skimAffiliateFees(ctx, mgr, msg.Tx, msg.Signer, msg.Tx.Memo)
		if err != nil {
			ctx.Logger().Error("fail to skim affiliate fees", "error", err)
		}
		// Reduce main swap amount by total distributed affiliate fee
		msg.Tx.Coins[0].Amount = common.SafeSub(msg.Tx.Coins[0].Amount, totalAffiliateFee)
	}

	// Queue the main swap
	if err := mgr.Keeper().SetSwapQueueItem(ctx, msg, 0); err != nil {
		ctx.Logger().Error("fail to add swap to queue", "error", err)
	}
}

// skimAffiliateFees - attempts to distribute the affiliate fee to the main affiliate in the memo and to each nested subaffiliate.
// Returns the total fee distributed priced in inboundCoin.Asset.
// Logic:
// 1. Parse the memo to get the affiliate mayaname or address and the main affiliate fee
// 2. For affiliate and each subaffiliate
// - If inbound coin is CACAO transfer to the affiliate
// - If inbound coin is not CACAO, swap the coin to CACAO and transfer to the affiliate
// - If affiliate is a mayaname and has a preferred asset, send CACAO to the affiliate collector
func skimAffiliateFees(ctx cosmos.Context, mgr Manager, mainTx common.Tx, signer cosmos.AccAddress, memoStr string) (cosmos.Uint, error) {
	// Parse memo
	memo, err := ParseMemoWithMAYANames(ctx, mgr.Keeper(), memoStr)
	if err != nil {
		ctx.Logger().Error("fail to parse swap memo", "memo", memoStr, "error", err)
		return cosmos.ZeroUint(), err
	}
	affiliates := memo.GetAffiliates()
	affiliatesBps := memo.GetAffiliatesBasisPoints()
	if len(affiliates) == 0 || len(affiliatesBps) == 0 {
		return cosmos.ZeroUint(), nil
	}

	// initialize swapIndex for affiliate swaps (index 0 is reserved for the main swap)
	swapIndex := 1
	totalDistributed := cosmos.ZeroUint()
	totalSwapAmount := mainTx.Coins[0].Amount
	shares := calculateAffiliateShares(ctx, mgr, totalSwapAmount, affiliates, affiliatesBps)
	for _, share := range shares {
		if !share.amount.IsZero() {
			distributed := distributeShare(ctx, mgr, share, mainTx, signer, &swapIndex)
			totalDistributed = totalDistributed.Add(distributed)
		}
	}
	return totalDistributed, nil
}

// affiliate fee share dtsributor
type affiliateFeeShare struct {
	amount     cosmos.Uint
	nativeDest common.Address // mayaname alias/owner or explicit native affiliate address or NoAddress if preferred address is set
	mayaname   *types.MAYAName
}

func calculateAffiliateShares(ctx cosmos.Context, mgr Manager, inputAmount cosmos.Uint, affiliates []string, affiliatesBps []cosmos.Uint) []affiliateFeeShare {
	// construct a virtual mayaname so we can use the calculateNestedAffiliateShares function for the root affiliates as well
	virtualSubAffiliates := make([]types.MAYANameSubaffiliate, len(affiliates))
	for i := range affiliates {
		virtualSubAffiliates[i].Name = affiliates[i]
		virtualSubAffiliates[i].Bps = affiliatesBps[i]
	}
	virtualMayaname := NewMAYAName("", 0, nil, common.EmptyAsset, nil, cosmos.ZeroUint(), virtualSubAffiliates)
	return calculateNestedAffiliateShares(ctx, mgr, virtualMayaname, inputAmount)
}

func calculateNestedAffiliateShares(ctx cosmos.Context, mgr Manager, mayaname MAYAName, inputAmt cosmos.Uint) []affiliateFeeShare {
	keeper := mgr.Keeper()
	// get the direct subaffiliates of the MAYAName
	remainingAmt := inputAmt
	shares := make([]affiliateFeeShare, 0)
	for _, subAff := range mayaname.GetSubaffiliates() {
		subaffIsMayaname := keeper.MAYANameExists(ctx, subAff.Name)
		subaffExplicitAddress := common.NoAddress
		if !subaffIsMayaname {
			var err error
			if subaffExplicitAddress, err = FetchAddress(ctx, keeper, subAff.Name, common.BASEChain); err != nil {
				// remove the subaffiliate from subaffiliate list if invalid or not exists
				ctx.Logger().Info(fmt.Sprintf("invalid subaffiliate %s registered for %s, removing it", subAff.Name, mayaname.Name))
				mayaname.RemoveSubaffiliate(subAff.Name)
				keeper.SetMAYAName(ctx, mayaname)
				continue
			}
		}
		subAmt := common.GetSafeShare(subAff.Bps, cosmos.NewUint(constants.MaxBasisPts), inputAmt)
		ctx.Logger().Debug("affiliate share calculated", "(sub)affiliate", subAff.Name, "bps", subAff.Bps, "amount", subAmt)
		// if total subaffiliate shares exceed 100%, ignore them
		if subAmt.GT(remainingAmt) {
			break
		}
		remainingAmt = remainingAmt.Sub(subAmt)
		if subaffIsMayaname {
			subAffMn, err := keeper.GetMAYAName(ctx, subAff.Name)
			if err != nil {
				ctx.Logger().Error(fmt.Sprintf("fail to get sub-affiliate MAYAName %s for MAYAName %s", subAff.Name, mayaname.Name))
				continue
			}
			shares = append(shares, calculateNestedAffiliateShares(ctx, mgr, subAffMn, subAmt)...)
		} else {
			shares = append(shares, affiliateFeeShare{
				mayaname:   nil,
				amount:     subAmt,
				nativeDest: subaffExplicitAddress,
			})
		}
	}
	if mayaname.Name != "" { // if empty, it is the virtual mayaname
		nativeDest := common.NoAddress
		if mayaname.PreferredAsset.IsEmpty() {
			nativeDest = mayaname.GetAlias(common.BASEChain)
			if nativeDest.IsEmpty() {
				nativeDest = common.Address(mayaname.Owner.String())
				ctx.Logger().Info("affiliate MAYAName doesn't have native chain alias, owner will be used instead", "mayaname", mayaname.Name, "owner", nativeDest)
			}
		}
		shares = append(shares, affiliateFeeShare{
			mayaname:   &mayaname,
			amount:     remainingAmt,
			nativeDest: nativeDest,
		})
		// only for debug log
		address := nativeDest.String()
		if nativeDest.IsEmpty() {
			address = fmt.Sprintf("affCol/%s:%s", mayaname.PreferredAsset, mayaname.GetAlias(mayaname.PreferredAsset.Chain))
		}
		ctx.Logger().Debug("affiliate share added", "mayaname", mayaname.Name, "amount", remainingAmt, "address", address)
	}
	return shares
}

func distributeShare(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, mainTx common.Tx, signer cosmos.AccAddress, swapIndex *int) cosmos.Uint {
	var err error
	if mainTx.Coins[0].Asset.IsNativeBase() {
		err = sendShare(ctx, mgr, share, swapIndex)
	} else {
		err = swapShare(ctx, mgr, share, mainTx, signer, swapIndex)
	}
	if err != nil {
		// just log the error, fee distribution to other affiliates will continue
		var to string
		if share.mayaname != nil {
			to = share.mayaname.Name
		} else {
			to = share.nativeDest.String()
		}
		ctx.Logger().Error("fail to distribute affiliate fee share", "to", to, "error", err)
		return cosmos.ZeroUint()
	}
	return share.amount
}

func sendShare(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	coin := common.NewCoin(common.BaseNative, share.amount)
	if !share.nativeDest.IsEmpty() {
		// either no mayaname or no preferred asset
		// send cacao to nativeDest (mayaname alias/owner or explicit aff address)
		toAccAddress, err := share.nativeDest.AccAddress()
		if err != nil {
			return fmt.Errorf("fail to convert address into AccAddress, address: %s, error: %w", share.nativeDest, err)
		}
		// only for debug log
		mayaname := "n/a"
		if share.mayaname != nil {
			mayaname = share.mayaname.Name
		}
		ctx.Logger().Info("sending affiliate fee to affiliate address", "amount", share.amount, "mayaname", mayaname, "address", share.nativeDest)
		sdkErr := mgr.Keeper().SendFromModuleToAccount(ctx, AsgardName, toAccAddress, common.NewCoins(coin))
		if sdkErr != nil {
			return fmt.Errorf("fail to send native asset to affiliate, address: %s, error: %w", share.nativeDest, sdkErr)
		}
	} else {
		// preferred asset provided
		// send cacao to affiliate collector
		ctx.Logger().Info("sending affiliate fee to affiliate collector", "amount", share.amount, "mayaname", share.mayaname.Name)
		err := updateAffiliateCollector(ctx, mgr, share, swapIndex)
		if err != nil {
			return fmt.Errorf("failed to send funds to affiliate collector, error: %w", err)
		}
	}
	return nil
}

func swapShare(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, mainTx common.Tx, signer cosmos.AccAddress, swapIndex *int) error {
	// Copy mainTx coins so as not to modify the original
	mainTx.Coins = mainTx.Coins.Copy()

	// Construct preferred asset swap tx
	affSwapMsg := NewMsgSwap(
		mainTx,
		common.BaseAsset(),
		share.nativeDest, // mayaname alias/owner or explicit aff address or NoAddress if preferred address is set
		cosmos.ZeroUint(),
		common.NoAddress,
		cosmos.ZeroUint(),
		"", "", nil,
		MarketOrder,
		0, 0,
		signer,
	)

	affSwapMsg.Tx.Coins[0].Amount = share.amount

	// if nativeDest is empty that means preferred asset is set
	if share.nativeDest.IsEmpty() {
		// Set AffiliateCollector Module as destination (toAddress) and populate the AffiliateAddress
		// so that the swap handler can increment the emitted CACAO for the affiliate in the AffiliateCollector
		affiliateColllector, err := mgr.Keeper().GetModuleAddress(AffiliateCollectorName)
		if err != nil {
			return err
		}
		affSwapMsg.AffiliateAddress = common.Address(share.mayaname.Owner.String())
		affSwapMsg.Destination = affiliateColllector // affiliate collector module address

		// trigger preferred asset swap if needed
		checkAndTriggerPreferredAssetSwap(ctx, mgr, share, swapIndex)
	}

	// only for debug log
	mayaname := "n/a"
	if share.mayaname != nil {
		mayaname = share.mayaname.Name
	}
	ctx.Logger().Debug("affiliate fee swap queue", "index", *swapIndex, "amount", share.amount, "mayaname", mayaname, "address", affSwapMsg.Destination)
	// swap the affiliate fee
	if err := mgr.Keeper().SetSwapQueueItem(ctx, *affSwapMsg, *swapIndex); err != nil {
		return fmt.Errorf("fail to add swap to queue, error: %w", err)
	}
	*swapIndex++

	return nil
}

func checkAndTriggerPreferredAssetSwap(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) {
	affCol, err := mgr.Keeper().GetAffiliateCollector(ctx, share.mayaname.Owner)
	if err != nil {
		ctx.Logger().Error("failed to get affiliate collector", "MAYAName", share.mayaname.Name, "error", err)
		return
	}
	// Trigger a preferred asset swap if the accrued CACAO exceeds the threshold:
	// Threshold = PreferredAssetOutboundFeeMultiplier (default = 100) x current outbound fee of the preferred asset chain.
	// If the affiliate collector's CACAO amount exceeds the threshold, initiate the preferred asset swap.
	threshold := getPreferredAssetSwapThreshold(ctx, mgr, share.mayaname.PreferredAsset)
	if affCol.CacaoAmount.GT(threshold) {
		ctx.Logger().Info("preferred asset swap triggered", "mayaname", share.mayaname.Name, "prefAsset", share.mayaname.PreferredAsset, "threshold", threshold, "affcol amount", affCol.CacaoAmount)
		if err = triggerPreferredAssetSwap(ctx, mgr, *share.mayaname, affCol, *swapIndex); err != nil {
			ctx.Logger().Error("fail to swap to preferred asset", "mayaname", share.mayaname.Name, "err", err)
		} else {
			*swapIndex++
		}
	} else {
		ctx.Logger().Info("preferred asset swap not yet triggered", "mayaname", share.mayaname.Name, "prefAsset", share.mayaname.PreferredAsset, "threshold", threshold, "affcol amount", affCol.CacaoAmount)
	}
}

func updateAffiliateCollector(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.112.0")):
		return updateAffiliateCollectorV112(ctx, mgr, share, swapIndex)
	default:
		return nil
	}
}

func updateAffiliateCollectorV112(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	affCol, err := mgr.Keeper().GetAffiliateCollector(ctx, share.mayaname.Owner)
	if err != nil {
		return fmt.Errorf("failed to get affiliate collector, MAYAName %s, error: %w", share.mayaname.Name, err)
	}
	coin := common.NewCoin(common.BaseNative, share.amount)
	if err := mgr.Keeper().SendFromModuleToModule(ctx, AsgardName, AffiliateCollectorName, common.NewCoins(coin)); err != nil {
		return fmt.Errorf("failed to send funds to affiliate collector, error: %w", err)
	} else {
		affCol.CacaoAmount = affCol.CacaoAmount.Add(coin.Amount)
		mgr.Keeper().SetAffiliateCollector(ctx, affCol)
	}
	checkAndTriggerPreferredAssetSwap(ctx, mgr, share, swapIndex)
	return nil
}
