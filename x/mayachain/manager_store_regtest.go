//go:build regtest
// +build regtest

package mayachain

import (
	"fmt"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

func migrateStoreV86(ctx cosmos.Context, mgr *Mgrs)    {}
func migrateStoreV88(ctx cosmos.Context, mgr Manager)  {}
func migrateStoreV90(ctx cosmos.Context, mgr Manager)  {}
func migrateStoreV96(ctx cosmos.Context, mgr Manager)  {}
func migrateStoreV102(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV104(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV105(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV106(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV107(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV108(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV109(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV110(ctx cosmos.Context, mgr Manager) {}

func migrateStoreV111(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v111", "error", err)
		}
	}()

	// For any in-progress streaming swaps to non-RUNE Native coins,
	// mint the current Out amount to the Pool Module.
	var coinsToMint common.Coins

	iterator := mgr.Keeper().GetSwapQueueIterator(ctx)
	defer iterator.Close()
	for ; iterator.Valid(); iterator.Next() {
		var msg MsgSwap
		if err := mgr.Keeper().Cdc().Unmarshal(iterator.Value(), &msg); err != nil {
			ctx.Logger().Error("fail to fetch swap msg from queue", "error", err)
			continue
		}

		if !msg.IsStreaming() || !msg.TargetAsset.IsNative() || msg.TargetAsset.IsBase() {
			continue
		}

		swp, err := mgr.Keeper().GetStreamingSwap(ctx, msg.Tx.ID)
		if err != nil {
			ctx.Logger().Error("fail to fetch streaming swap", "error", err)
			continue
		}

		if !swp.Out.IsZero() {
			mintCoin := common.NewCoin(msg.TargetAsset, swp.Out)
			coinsToMint = coinsToMint.Add(mintCoin)
		}
	}

	// The minted coins are for in-progress swaps, so keeping the "swap" in the event field and logs.
	var coinsToTransfer common.Coins
	for _, mintCoin := range coinsToMint {
		if err := mgr.Keeper().MintToModule(ctx, ModuleName, mintCoin); err != nil {
			ctx.Logger().Error("fail to mint coins during swap", "error", err)
		} else {
			// MintBurn event is not currently implemented, will ignore

			// mintEvt := NewEventMintBurn(MintSupplyType, mintCoin.Asset.Native(), mintCoin.Amount, "swap")
			// if err := mgr.EventMgr().EmitEvent(ctx, mintEvt); err != nil {
			// 	ctx.Logger().Error("fail to emit mint event", "error", err)
			// }
			coinsToTransfer = coinsToTransfer.Add(mintCoin)
		}
	}

	if err := mgr.Keeper().SendFromModuleToModule(ctx, ModuleName, AsgardName, coinsToTransfer); err != nil {
		ctx.Logger().Error("fail to move coins during swap", "error", err)
	}
}

func migrateStoreV112(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v112", "error", err)
		}
	}()

	// Mock ObservedTxVoter state from
	// https://mayanode.mayachain.info/mayachain/tx/details/6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB
	tx6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB := types.ObservedTxVoter{
		TxID: common.TxID("6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB"),
		Tx: types.ObservedTx{
			Tx: common.Tx{
				ID:          "6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
				Chain:       common.THORChain,
				FromAddress: common.Address("thor1kfqzcr8m73qd97z46wha2w8u6f22tj9dxyquj6"),
				Coins: common.Coins{
					{
						Asset:    common.RUNEAsset,
						Amount:   cosmos.NewUint(2000000000000),
						Decimals: 8,
					},
				},
				Gas: common.Gas{
					{
						Asset:  common.RUNEAsset,
						Amount: cosmos.NewUint(2000000),
					},
				},
				Memo: "=:ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48:0xAa287489e76B11B56dBa7ca03e155369400f3d65:9749038937200/3/0:ts:50",
			},
			ObservedPubKey: "tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx",
			Signers:        []string{""},
			OutHashes:      []string{"0000000000000000000000000000000000000000000000000000000000000000"},
			Status:         types.Status_done,
		},
		Txs: types.ObservedTxs{
			{
				Tx: common.Tx{
					ID:          "6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
					Chain:       common.THORChain,
					FromAddress: common.Address("thor1kfqzcr8m73qd97z46wha2w8u6f22tj9dxyquj6"),
					Coins: common.Coins{
						{
							Asset:    common.RUNEAsset,
							Amount:   cosmos.NewUint(2000000000000),
							Decimals: 8,
						},
					},
					Gas: common.Gas{
						{
							Asset:  common.RUNEAsset,
							Amount: cosmos.NewUint(2000000),
						},
					},
					Memo: "=:ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48:0xAa287489e76B11B56dBa7ca03e155369400f3d65:9749038937200/3/0:ts:50",
				},
				ObservedPubKey: "tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx",
				Signers:        []string{""},
				OutHashes:      []string{"0000000000000000000000000000000000000000000000000000000000000000"},
				Status:         types.Status_done,
			},
		},
		Actions: []types.TxOutItem{
			{
				Chain:     common.BASEChain,
				ToAddress: common.Address("maya1rm0xppz0ypgr3zqymnrdtnnjs4kpxqgy8tfmuk"),
				Coin: common.Coin{
					Asset:  common.BaseAsset(),
					Amount: cosmos.NewUint(8888139344845),
				},
				Memo: "OUT:6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
				MaxGas: common.Gas{
					{
						Asset:    common.BaseAsset(),
						Amount:   cosmos.ZeroUint(),
						Decimals: 8,
					},
				},
				GasRate: 2000000000,
				InHash:  common.TxID("6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB"),
			},
		},
		OutTxs: common.Txs{
			{
				ID:        common.TxID("0000000000000000000000000000000000000000000000000000000000000000"),
				Chain:     common.BASEChain,
				ToAddress: common.Address("maya1rm0xppz0ypgr3zqymnrdtnnjs4kpxqgy8tfmuk"),
				Coins: common.Coins{
					{
						Asset:  common.BaseAsset(),
						Amount: cosmos.NewUint(8888139344845),
					},
				},
				Gas: common.Gas{
					{
						Asset:  common.BaseAsset(),
						Amount: cosmos.NewUint(2000000000),
					},
				},
				Memo: "OUT:6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
			},
		},
		FinalisedHeight: 1,
		UpdatedVault:    true,
	}
	mgr.Keeper().SetObservedTxInVoter(ctx, tx6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB)

	// https://mayanode.mayachain.info/mayachain/tx/details/80559CC3CCF2665531AAA7DD6B59F986721C6B76F1DD056DAE58DCC4878C5D56
	// Tx doesn't have planned "actions" nor "out_txs", send it with TryAddTxOutItem()
	var err error
	originalTxID := "80559CC3CCF2665531AAA7DD6B59F986721C6B76F1DD056DAE58DCC4878C5D56"
	maxGas, err := mgr.gasMgr.GetMaxGas(ctx, common.THORChain)
	if err != nil {
		ctx.Logger().Error("unable to GetMaxGas while retrying issue 1", "err", err)
	} else {
		gasRate := mgr.gasMgr.GetGasRate(ctx, common.THORChain)
		droppedRescue := types.TxOutItem{
			Chain:       common.THORChain,
			ToAddress:   common.Address("thor1sucvdnzcf4j6ynep4n4skjpq8tvqv8ags3a4ky"),
			VaultPubKey: common.PubKey("tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx"), // regtests pubkey
			Coin: common.NewCoin(
				common.RUNEAsset,
				// Check out value for https://mayanode.mayachain.info/mayachain/block?height=8292361
				cosmos.NewUint(uint64(199757582857)),
			),
			Memo:    fmt.Sprintf("OUT:%s", originalTxID),
			InHash:  common.TxID(originalTxID),
			GasRate: int64(gasRate.Uint64()),
			MaxGas:  common.Gas{maxGas},
		}

		ok, err := mgr.txOutStore.TryAddTxOutItem(ctx, mgr, droppedRescue, cosmos.ZeroUint())
		if err != nil {
			ctx.Logger().Error("fail to retry THOR rescue tx", "error", err)
		}
		if !ok {
			ctx.Logger().Error("TryAddTxOutItem didn't success for tx")
		}
	}

	// https://mayanode.mayachain.info/mayachain/tx/details/6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB
	// Tx have planned "actions" but doesn't have "out_txs", send it with TryAddTxOutItem()
	originalTxID = "6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB"
	maxGas, err = mgr.gasMgr.GetMaxGas(ctx, common.ETHChain)
	if err != nil {
		ctx.Logger().Error("unable to GetMaxGas while retrying issue 1", "err", err)
	} else {
		gasRate := mgr.gasMgr.GetGasRate(ctx, common.ETHChain)
		droppedRescue := types.TxOutItem{
			Chain:       common.ETHChain,
			ToAddress:   common.Address("0xAa287489e76B11B56dBa7ca03e155369400f3d65"),
			VaultPubKey: common.PubKey("tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx"), // regtests pubkey
			Coin: common.NewCoin(
				common.USDCAsset,
				// Check out value for https://mayanode.mayachain.info/mayachain/block?height=8292990
				cosmos.NewUint(uint64(9861109874700)),
			),
			Memo:    fmt.Sprintf("OUT:%s", originalTxID),
			InHash:  common.TxID(originalTxID),
			GasRate: int64(gasRate.Uint64()),
			MaxGas:  common.Gas{maxGas},
		}

		ok, err := mgr.txOutStore.TryAddTxOutItem(ctx, mgr, droppedRescue, cosmos.ZeroUint())
		if err != nil {
			ctx.Logger().Error("fail to retry THOR rescue tx", "error", err)
		}
		if !ok {
			ctx.Logger().Error("TryAddTxOutItem didn't success for tx")
		}
	}
}
