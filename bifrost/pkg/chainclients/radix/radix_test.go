package radix

import (
	"encoding/json"
	"math/big"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/cosmos/cosmos-sdk/crypto/hd"
	cKeys "github.com/cosmos/cosmos-sdk/crypto/keyring"
	ctypes "gitlab.com/mayachain/binance-sdk/common/types"
	. "gopkg.in/check.v1"

	"gitlab.com/mayachain/mayanode/bifrost/mayaclient"
	"gitlab.com/mayachain/mayanode/bifrost/metrics"
	"gitlab.com/mayachain/mayanode/bifrost/pubkeymanager"
	"gitlab.com/mayachain/mayanode/cmd"
	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/config"
	ttypes "gitlab.com/mayachain/mayanode/x/mayachain/types"
)

const (
	bob      = "bob"
	password = "password"
)

func TestPackage(t *testing.T) { TestingT(t) }

type radixTestFixtures struct {
	transactionPreview string
	networkStatus      string
}

type RadixTestSuite struct {
	client   *Client
	server   *httptest.Server
	bridge   mayaclient.MayachainBridge
	cfg      config.BifrostChainConfiguration
	m        *metrics.Metrics
	keys     *mayaclient.Keys
	fixtures struct {
		transactionPreview string
		networkStatus      string
	}
}

var _ = Suite(&RadixTestSuite{})

var m *metrics.Metrics

func getMetricForTest(c *C) *metrics.Metrics {
	if m == nil {
		var err error
		m, err = metrics.NewMetrics(config.BifrostMetricsConfiguration{
			Enabled:      false,
			ListenPort:   9000,
			ReadTimeout:  time.Second,
			WriteTimeout: time.Second,
			Chains:       common.Chains{common.XRDChain},
		})
		c.Assert(m, NotNil)
		c.Assert(err, IsNil)
	}
	return m
}

func (s *RadixTestSuite) SetUpSuite(c *C) {
	ttypes.SetupConfigForTest()
	kb := cKeys.NewInMemory()
	_, _, err := kb.NewMnemonic(bob, cKeys.English, cmd.BASEChainHDPath, password, hd.Secp256k1)
	c.Assert(err, IsNil)
	s.keys = mayaclient.NewKeysWithKeybase(kb, bob, password)
}

func (s *RadixTestSuite) SetUpTest(c *C) {
	s.m = getMetricForTest(c)
	s.cfg = config.BifrostChainConfiguration{
		ChainID:     "XRD",
		UserName:    bob,
		Password:    password,
		DisableTLS:  true,
		HTTPostMode: true,
		BlockScanner: config.BifrostBlockScannerConfiguration{
			StartBlockHeight:   1,
			HTTPRequestTimeout: 30 * time.Second,
		},
	}

	ns := strconv.Itoa(time.Now().Nanosecond())
	ctypes.Network = ctypes.TestNetwork

	thordir := filepath.Join(os.TempDir(), ns, ".thorcli")
	cfg := config.BifrostClientConfiguration{
		ChainID:         "mayachain",
		ChainHost:       "http://localhost",
		SignerName:      bob,
		SignerPasswd:    password,
		ChainHomeFolder: thordir,
	}

	s.server = httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		switch req.RequestURI {
		case "/":
			r := struct {
				Method string   `json:"method"`
				Params []string `json:"params"`
			}{}
			_ = json.NewDecoder(req.Body).Decode(&r)

			if r.Method == "createwallet" {
				_, err := rw.Write([]byte(`{ "result": null, "error": null, "id": 1 }`))
				c.Assert(err, IsNil)
			}
		case "/mayachain/vaults/pubkeys":
			httpTestHandler(c, rw, "../../../../test/fixtures/endpoints/vaults/pubKeys.json")
		case "/mayachain/mimir/key/MaxUTXOsToSpend":
			_, err := rw.Write([]byte(`-1`))
			c.Assert(err, IsNil)
		case "/status/network-configuration":
			httpTestHandler(c, rw, "../../../../test/fixtures/xrd/network_configuration.json")
		case "/status/network-status":
			httpTestHandler(c, rw, s.fixtures.networkStatus)
		case "/transaction/preview":
			httpTestHandler(c, rw, s.fixtures.transactionPreview)
		default:
			panic("No handler found for " + req.RequestURI)
		}
	}))

	var err error
	cfg.ChainHost = s.server.Listener.Addr().String()
	s.bridge, err = mayaclient.NewMayachainBridge(cfg, s.m, s.keys)
	c.Assert(err, IsNil)
	s.cfg.RPCHost = "http://" + s.server.Listener.Addr().String()

	var pubkeyMgr *pubkeymanager.PubKeyManager
	pubkeyMgr, err = pubkeymanager.NewPubKeyManager(s.bridge, s.m)
	if err != nil {
		log.Fatal().Err(err).Msg("fail to create pubkey manager")
	}
	if err = pubkeyMgr.Start(); err != nil {
		log.Fatal().Err(err).Msg("fail to start pubkey manager")
	}

	s.client, err = NewClient(s.keys, s.cfg, nil, s.bridge, s.m, pubkeyMgr)
	c.Assert(err, IsNil)
	c.Assert(s.client, NotNil)
}

func (s *RadixTestSuite) TestGetAccount(c *C) {
	s.fixtures = radixTestFixtures{
		transactionPreview: "../../../../test/fixtures/xrd/transaction_preview.json",
		networkStatus:      "../../../../test/fixtures/xrd/network_status.json",
	}
	pubkey := ttypes.GetRandomPubKey()
	acct, err := s.client.GetAccount(pubkey, big.NewInt(0))
	c.Assert(err, IsNil)
	c.Assert(acct.Coins, HasLen, 1)
	c.Assert(acct.Coins[0].Amount.BigInt().Cmp(big.NewInt(102019290166504)) == 0, Equals, true)
}

func (s *RadixTestSuite) TestGetAccountInsufficientFee(c *C) {
	s.fixtures = radixTestFixtures{
		transactionPreview: "../../../../test/fixtures/xrd/transaction_preview_insufficient_fee.json",
		networkStatus:      "../../../../test/fixtures/xrd/network_status_insufficient_fee.json",
	}
	pubkey := ttypes.GetRandomPubKey()
	acct, err := s.client.GetAccount(pubkey, big.NewInt(0))
	c.Assert(err, NotNil)
	c.Assert(acct.Coins, HasLen, 0)
	c.Assert(err.Error(), Equals, "failed to get vault XRD balance: method call preview failed: method preview failed with status: Rejected, the lock fee account is out of funds to simulate the tx fee which is required to get the vault balance")
}

func httpTestHandler(c *C, rw http.ResponseWriter, fixture string) {
	content, err := os.ReadFile(fixture)
	if err != nil {
		c.Fatal(err)
	}
	rw.Header().Set("Content-Type", "application/json")
	if _, err := rw.Write(content); err != nil {
		c.Fatal(err)
	}
}
