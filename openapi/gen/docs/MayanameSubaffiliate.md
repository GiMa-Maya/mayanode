# MayanameSubaffiliate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Bps** | Pointer to **int64** |  | [optional] 

## Methods

### NewMayanameSubaffiliate

`func NewMayanameSubaffiliate() *MayanameSubaffiliate`

NewMayanameSubaffiliate instantiates a new MayanameSubaffiliate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMayanameSubaffiliateWithDefaults

`func NewMayanameSubaffiliateWithDefaults() *MayanameSubaffiliate`

NewMayanameSubaffiliateWithDefaults instantiates a new MayanameSubaffiliate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *MayanameSubaffiliate) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *MayanameSubaffiliate) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *MayanameSubaffiliate) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *MayanameSubaffiliate) HasName() bool`

HasName returns a boolean if a field has been set.

### GetBps

`func (o *MayanameSubaffiliate) GetBps() int64`

GetBps returns the Bps field if non-nil, zero value otherwise.

### GetBpsOk

`func (o *MayanameSubaffiliate) GetBpsOk() (*int64, bool)`

GetBpsOk returns a tuple with the Bps field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBps

`func (o *MayanameSubaffiliate) SetBps(v int64)`

SetBps sets Bps field to given value.

### HasBps

`func (o *MayanameSubaffiliate) HasBps() bool`

HasBps returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


